"""
puredata / py script
Take bangs on inlets and send to motors via surpreme sensorimotor

2018, Oswald Berthold
"""
try:
    import pyext
except:
    print "ERROR: This script must be loaded by the PD/Max pyext external"

import os

# print os.environ
    
from sensorimotor.sensorimotor import Sensorimotor
from time import sleep

class ev2sm_(pyext._class):
    _inlets = 2
    _outlets = 1

class ev2sm(pyext._class):
    _inlets = 2
    _outlets = 1

    # constructor
    def __init__(self,*args):
	# if len(args) == 1: 
	#     if isNumber(args[0]):
	# 	self.tmp = args[0]
	#     else:
	# 	print "ex3: __init__ has superfluous arguments"
        self.motors = Sensorimotor(number_of_motors = 2, verbose = False)
        # checking for motors
        print("Scanning sensorimotors.".format())
        self.N = self.motors.ping()
        print("Found {0} sensorimotors.".format(self.N))
        sleep(1.0)

        # starting motorcord
        # self.motors.set_voltage_limit([1.0, 1.0])
        self.motors.set_voltage_limit([1.0, 1.0])
        self.motors.start()
        
    def __del__(self,*args):
        self.motors.stop()
        
    # try:
    #     while(True):
    #         # USER CODE HERE BEGIN

    #         for i in range(16):
    #             motors.apply_impulse([i * 0.05 for _ in range(2)])
    #             sleep(0.2)

    def bang_1(self):
        print "bang into first inlet"
        self.motors.apply_impulse([0.8, 0.8])
        
    def int_1(self,f):
	print "Integer",f,"into first inlet"

    def float_1(self,f):
	print "Float",f,"into first inlet"

    def list_1(self,*s):
        motorval = list(s)
	# print "motorval = ",len(s),type(s),s,"into first inlet"
        print "motorval", motorval
        self.motors.apply_impulse(motorval)

    def connect_2(self, *args):
        # sleep(1.0)

        # checking for motors
        print("Scanning sensorimotors.".format())
        self.N = self.motors.ping()
        print("Found {0} sensorimotors.".format(self.N))
        sleep(1.0)

        # starting motorcord
        self.motors.set_voltage_limit([0.5, 1.0])

    def start_2(self, *args):
        self.motors.start()

    def stop_2(self, *args):
        self.motors.stop()
