Beat tracking
=============

Beat tracking modules for event-based (MIDI, OSC, ...) and audio (file, live) input.

Core element is a synchronization mechanism. Assuming regular ticks, the problem is to estimate the tempo (frequency) and the phase (phase), down to the inherent uncertainty in the microtiming.

Works as is but can be augmented by additional information such as

-   a beat/no-beat classifier
-   down beat estimate
-   musical genre classification
-   event density prior
-   and so on

Ideally we want to have event-based timing-aware Bayesian ensemble combination of multiple models.

Algorithms
----------

| name                | platform | impl/tested | notes                          |
|---------------------|----------|-------------|--------------------------------|
| aubiotempo          | pd       | x / .       | from pd-aubio wrapping aubio   |
| rhythm\_estimator   | pd       | x / .       | from ...                       |
| beat                | pd       |             | maxlib                         |
| rhythm              | pd       |             | maxlib                         |
| librosa.beat\_track | py       | x / .       | beat\_tracking\_rt\_librosa.py |
|                     |          |             |                                |

Using puredata, also known as pd, and Python to quickly access existing implementations of different algorithms. Pd allows to easily do standard audio and event processing tasks and allows to focus on the relevant functions.

Recommended and tested setup is an Ubuntu 18.04 LTS where pd can be installed with

``` example
sudo apt install puredata puredata-extra
```

and pd-aubio similarly by running

``` example
sudo apt install pd-3dp pd-aubio pd-beatpipe pd-libdir pd-mjlib
```

which provides the aubio library's functions within pd.

The \`rhythm\_estimator\` algorithm is more work to install. Pd has a plugin API and plugins are called externals. There exists a large user contributed collection of externals including rhythm\_estimator, which we will

``` example
git clone https://git.purrdata.net/x75/purr-data.git
cd purr-data/externals/rhythm_estimator
./configure
make
make install
```

If all went well, you can now run the patches with

``` example
pd -path /usr/lib/pd/extra/rhythm_estimator -open pd/beattrack-src.pd
```

which opens pd and a patch window with boxes named "beattrack-aubio", "beattrack-rhythm<sub>estimator</sub>", "beattrack-src-adc", and "beattrack-src-file". You can click on these boxes and they will open to reveal subpatches with the corresponding functionality.

In the main window you can switch between the two tracking algorithms and live or file audio input. If the installation of rhythm\_estimator didn't succeed right away you can still run the aubio algorithm to start experimenting.

For the Python version with librosa you just run

``` example
python3 py/beat_tracking_rt_librosa.py
```

Maybe /path/to/beat<sub>tracking</sub>/py must be added to PYTHONPATH first.

The hardware interface to the drumming box uses libsensorimotor from suprememachines. You can get it by

``` example
git clone git@github.com:suprememachines/libsensorimotor.git
```

You will need to add /path/to/libsensorimotor/bin to your PYTHONPATH again. Then you can start the drummer-in-a-box.py to listen for commands from beat detection or rhythm generation respectively.

There's also a Python script in the pd directory to control the drumbox directly from pd using the python external. This is a bit hacky, because pyext behaves a bit weird and hangs pd on exit. A better way would be to do it through libpd and pylibpd.
