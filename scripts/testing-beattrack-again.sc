BeatTrack2

//required for MFCCs used below
b = Buffer.alloc(s,1024,1); //for sampling rates 44100 and 48000
//b = Buffer.alloc(s,2048,1); //for sampling rates 88200 and 96000

//this is a one minute pop song; you should load something equivalent for testing
d=Buffer.read(s,"/home/x75/trk012-5-mono.wav");
d=Buffer.read(s,"/Volumes/data/stevebeattrack/samples/019.wav");


//very feature dependent
(
a= SynthDef(\help_beattrack2_1,{arg vol=1.0, beepvol=1.0, lock=0;
var in, kbus;
var trackb,trackh,trackq,tempo, phase, period, groove;
var bsound,hsound,qsound, beep;
var fft;
var feature1, feature2, feature3;

in= PlayBuf.ar(1,d.bufnum,BufRateScale.kr(d.bufnum),1,0,1);
//in = SoundIn.ar(0);

//Create some features
fft = FFT(b.bufnum, in);

feature1= Onsets.kr(fft,odftype:\mkl, rawodf:1);

feature2= Onsets.kr(fft,odftype:\complex, rawodf:1);//two coefficients

kbus= Out.kr(0, [feature1,feature2]);

	// feature1= RunningSum.rms(in,64);
	// feature2= MFCC.kr(fft,2); //two coefficients
	// feature3= A2K.kr(LPF.ar(in,1000));
	//
	// kbus= Out.kr(0, [feature1, feature3]++feature2);

//Look at four features
#trackb,trackh,trackq,tempo, phase, period, groove=BeatTrack2.kr(0,4,2.0, 0.02, lock, -2.5);

beep= SinOsc.ar(1000,0.0,Decay.kr(trackb,0.1));

Out.ar(0,Pan2.ar((vol*in)+(beepvol*beep),0.0));
}).play
)


//you can also test at 48000 and it should work
(
a= SynthDef(\help_beattrack,{arg vol=1.0, beepvol=1.0, lock=0;
var in, fft, resample;
var trackb,trackh,trackq,tempo;
var bsound,hsound,qsound, beep;

in= PlayBuf.ar(1,d,BufRateScale.kr(d),1,0,1);
//in = SoundIn.ar(0);

fft = FFT(b, in);

#trackb,trackh,trackq,tempo=BeatTrack.kr(fft, lock);

beep= SinOsc.ar(1000,0.0,Decay.kr(trackb,0.1));

Out.ar(0,Pan2.ar((vol*in)+(beepvol*beep),0.0));
}).play
)