#!/bin/bash

# start the monkeydrummer demo

screen -dmS monkey -t jackd sh -c "jackd -d alsa -d hw:1 -r 48000 -rt -p 128 -n 2"
sleep 2
# screen -S monkey -X screen screen -t pd sh -c "cd /home/src/QK/smp_beat_tracking/pd; pd -nogui -alsamidi -midiindev 1 -midioutdev 1 -path /home/src/supercollider/marsyas/build/lib beattrack0.pd"
screen -S monkey -X screen screen -t pd sh -c "cd /home/src/QK/smp_beat_tracking/pd; pd -gui -alsamidi -midiindev 1 -midioutdev 1 -path /home/src/supercollider/marsyas/build/lib beattrack0.pd"
sleep 2
screen -S monkey -X screen screen -t mocp sh -c "mocp"
sleep 1
screen -S monkey -X screen screen -t connect "/home/pi/bin/monkey_connect.sh"
