#!/bin/bash

mocp --play --append /home/lib/audio/work/miniclub/pkg-[45]

for i in 0 1 ; do
    # echo moc:output${i} pure_data:input${i}
    jack_connect moc:output${i} pure_data:input${i}
done

jack_disconnect moc:output0 system:playback_1
jack_disconnect moc:output1 system:playback_2

aconnect 129:1 20:0

oscsend osc.udp://localhost:7778 /algo i 3

