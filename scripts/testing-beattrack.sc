Event

b = Buffer.alloc(s,1024,1); //for sampling rates 44100 and 48000

(
SynthDef(\help_beaMttrack2,{
var trackb,trackh,trackq,tempo;
var source;
var bsound,hsound,qsound;

source= SoundIn.ar(0);

#trackb,trackh,trackq,tempo=BeatTrack.kr(FFT(b, source));

bsound= Pan2.ar(LPF.ar(WhiteNoise.ar*(Decay.kr(trackb,0.05)),1000),0.0);

hsound= Pan2.ar(BPF.ar(WhiteNoise.ar*(Decay.kr(trackh,0.05)),3000,0.66),-0.5);

qsound= Pan2.ar(HPF.ar(WhiteNoise.ar*(Decay.kr(trackq,0.05)),5000),0.5);

	// Out.ar(0, bsound+hsound+qsound);
Out.ar(0, bsound+hsound);
}).play;
)

//favour higher tempi in own weighting scheme
(
c=Array.fill(120,{arg i; 0.5+(0.5*(i/120))});
e=Buffer.sendCollection(s, c, 1);
)


(
SynthDef(\help_beattrack2_2,{
var trackb,trackh,trackq,tempo;
var source, kbus;
var bsound,hsound,qsound;

source= SoundIn.ar(0, 9.dbamp); //PlayBuf.ar(1,d.bufnum,BufRateScale.kr(d.bufnum),1,0,1);
	source.poll;

//downsampling automatic via kr from ar
kbus= Out.kr(0, LPF.ar(source,1000)); //([feature1, feature3]++feature2);

#trackb,trackh,trackq,tempo=BeatTrack2.kr(0,1,weightingscheme:b.bufnum);

bsound= Pan2.ar(LPF.ar(WhiteNoise.ar*(Decay.kr(trackb,0.05)),1000),0.0);

hsound= Pan2.ar(BPF.ar(WhiteNoise.ar*(Decay.kr(trackh,0.05)),3000,0.66),-0.5);

qsound= Pan2.ar(HPF.ar(WhiteNoise.ar*(Decay.kr(trackq,0.05)),5000),0.5);

	// Out.ar(0, source + bsound+hsound+qsound);
Out.ar(0, bsound);
}).play;
)


s.makeGui
Server.makeGui