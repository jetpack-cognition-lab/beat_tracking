import mido
import time
from classes import *
import threading
from queue import Queue

""" communication variables """

tempo_phase = [0.5, time.time(), time.time()]
beats = Buffer(2)
wait = [0.5]
ticks = Buffer(2)
offset = [0]
tempos  =[0]

""" first thread reads and stores the beats """

def min_abs(a,b) :
    if abs(a) > abs(b) :
        return b
    else :
        return a

def input_handler(buff, tempo_phase) :
    oxygen = 'Périphérique MIDI USB'
    input = mido.open_input(oxygen)
    input.receive()
    print("inital message received")
    old_now = time.time()
    while True :
        for msg in input :
            now = time.time()
            print(wait[0])
            print(offset[0])
            if now - buff.get_last() > 2 :  # reset after 2s without beat
                buff.reset()
            if msg.type == "note_on" :
                buff.update(now)
                # tempo_phase[2] = now

def phase_handler(offset, ticks) :
    oxygen = "Périphérique MIDI USB"
    input = mido.open_input(oxygen)
    input.receive()
    print("inital message received")
    old_now = time.time()
    while True :
        for msg in input :
            now = time.time()
            if msg.type == "note_on" :
                offset[0] = (min_abs(now-ticks.get_last(), now-ticks.get_first()))
        sleep(0.2)
        offset[0] = 0


def buffer_handler(buff, tempo_phase, wait, tempos) :
    while True :
        if buff.is_full() :
            times = buff.get_list()
            deltas = [round(times[i+1]-times[i],5) for i in range(len(times)-1)]
            tempo = np.mean(deltas)
            curr_tempo = np.mean(tempos)
            # print(abs((tempo-curr_tempo)/tempo))
            if abs((tempo-curr_tempo)/tempo) > 0.05 :
                tempos = [tempo]
                print("new tempo detected")
            else :
                tempos.append(tempo)
            wait[0] = np.mean(tempos)
            time.sleep(0.1)
            
        else :
            print("please input tempo")
            while not buff.is_full() :
                pass
                

def output_handler(buff, tempo_phase, wait, offset, ticks) :
    output = mido.open_output("test virtual_port")
    boom = mido.Message('note_on', note=100, velocity=10, time=1)
    while True :
        time.sleep(wait[0]-offset[0]+0.001)
        
        # ticks.update(round(time.time(),3))
        
        output.send(boom)
        ticks.update(round(time.time(),3))
        offset[0] = 0
            # tempo_phase[2] = time.time() - tempo_phase[2]
            
t1 = threading.Thread(target=input_handler, args = (beats,tempo_phase,))
t2 = threading.Thread(target=buffer_handler, args = (beats,tempo_phase, wait, tempos))
t3 = threading.Thread(target=output_handler, args = (beats,tempo_phase, wait, offset, ticks,))
t4 = threading.Thread(target=phase_handler, args = (offset, ticks,))
threads = [t1, t2, t3, t4]
active = threads[:]
# # 
# # 
for t in active :
    t.start()
    
for t in active :
    t.join()