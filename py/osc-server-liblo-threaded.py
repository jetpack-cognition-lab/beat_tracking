from __future__ import print_function
import liblo, sys, time, queue, pickle
import threading

# import numpy as np
# import matplotlib.pyplot as plt
# import mdp
# from sklearn.decomposition import PCA, IncrementalPCA, KernelPCA

from oscsrv import OSCsrv

if __name__ == '__main__':
    # create queue
    qu = queue.Queue(maxsize=10)
    
    # create server, listen on port 1234, use our queue
    oscsrv = OSCsrv(port=1234, queue=qu)

    # connect to pd and trigger reconnect to ourselves
    target = liblo.Address('osc.udp://192.168.188.27:1337')
    liblo.send(target, "/reconnect", 'bang')

    # prealloc numpy array to collect data
    # QUD = np.zeros((500, 38))

    # start loop
    cnt = 0
    try:
        while True:
            # read from the queue 
            while qu.qsize() > 0:
                # got one item
                qud = qu.get()
                # qud[1] = 0
                print('qu {0}'.format(qud))
                # QUD[0,:] = np.array(qud)
                # QUD = np.roll(QUD, shift=1, axis=0)

            cnt += 1
            # print loop status
            if cnt % 1000 == 0:
                print(f'cnt = {cnt}')
                
            # sleep
            time.sleep(1e-2)
            
    except KeyboardInterrupt:
        print("key fin")
        oscsrv.isrunning = False
        oscsrv.st.join()
