import mido
import time


class Midi_loop :
    """
    Class for recording and playing simple midi loops.
    """
    def __init__(self) :
        self.notes = []
        self.times = []
        self.tempo = 0

    def play_with_midi(self, output_port) :
        times = self.times
        notes = self.notes
        while True :
            i = 0
            for msg in notes : 
                output_port.send(msg)
                time.sleep(times[i])
                i += 1
                
    def play_with_robot(self) :
        """write here the code for interfacing with C++"""
        pass
    
    def record(self, input_port) :
        """
            Records in real time the midi messages
            from the given input (ex : Midi Keyboard)
            and stores them along with time intervals
        """
        notes = []
        times = []
        print("now recording")
        msg = ""
        for msg in input_port :
            now = time.time()
            if "note_on channel=14 note=60" in str(msg) :
                break
            notes.append(msg)
            times.append(now)
        times.append(now)
        intervals = [times[i]-times[i-1] for i in range(1, len(times))]
        self.notes = notes
        self.times = intervals
    
    
if __name__ == "__main__" :
    loop = midi_loop()
    loop.record(mido.open_input('Périphérique MIDI USB'))
    loop.play_with_midi(mido.open_output('test virtual_port'))

