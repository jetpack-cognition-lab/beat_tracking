from __future__ import print_function
import liblo, sys, time, queue, pickle
import threading
from pprint import pformat

# import numpy as np
# import matplotlib.pyplot as plt
# import mdp
# from sklearn.decomposition import PCA, IncrementalPCA, KernelPCA

# import matplotlib.pyplot as plt

import pickle

import numpy as np
# import pandas as pd
import librosa

# MLP for Pima Indians Dataset Serialize to JSON and HDF5
# from keras.models import Sequential
# from keras.layers import Dense
from keras.models import model_from_json

from smp_audio.common_librosa import data_stream_librosa, data_stream_get_librosa
from smp_audio.common_aubio import data_stream_aubio, data_stream_get_aubio


from oscsrv import OSCsrv


def model_unserialize(modelfile='model.json'):
    # load json and create model
    json_file = open(modelfile, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(modelfile[:-5] + ".h5")
    print("Loaded model from disk")
    return loaded_model


if __name__ == '__main__':
    # create queue
    qu = queue.Queue(maxsize=10)
    
    # create server, listen on port 1234, use our queue
    oscsrv = OSCsrv(port=1234, queue=qu)

    # connect to pd and trigger reconnect to ourselves
    target = liblo.Address('osc.udp://192.168.188.27:1337')
    liblo.send(target, "/reconnect", 'bang')

    osc_tgt_dmx = liblo.Address('osc.udp://192.168.188.35:7770')
    osc_msg_dmx = [0] * 50
    
    # prealloc numpy array to collect data
    # QUD = np.zeros((500, 38))

    modelfile = '/home/src/QK/smp_audio/notebooks/model.json'
    
    # load model
    model = model_unserialize(modelfile)
    print(f'model {model}')

    # load model training data
    # data = pd.read_csv('../notebooks/data-stream.csv')
    # data = data.drop(['filename'],axis=1)

    # genre_list = data.iloc[:, -1]
    # encoder = LabelEncoder()
    
    # scaler = StandardScaler()
    # X = scaler.fit_transform(np.array(data.iloc[:, :-1], dtype = float))
    scaler = pickle.load(open('/home/src/QK/smp_audio/notebooks/scaler.pkl', 'rb'))
    
    # load audio data
    framesize = 1024
    # src, sr = data_stream_librosa(
    #     filename=args.filename,
    #     frame_length=framesize,
    #     hop_length=framesize
    # )
    # X = []
    # Y = []
    sr = 48000

    def process_frame(blk_i, blk_y, model, scaler, framesize):
        # print(f'i {blk_i}, y {blk_y.shape}')

        if len(blk_y) < framesize: return
        # D_block = librosa.stft(block_y, n_fft=framesize, hop_length=framesize, center=False)
        # D.append(D_block[:,0])
        y = blk_y
        chroma_stft = librosa.feature.chroma_stft(y=y, sr=sr, n_fft=framesize, hop_length=framesize, center=False)
        rmse = librosa.feature.rms(y=y, frame_length=framesize, hop_length=framesize, center=False)
        spec_cent = librosa.feature.spectral_centroid(y=y, sr=sr, n_fft=framesize, hop_length=framesize, center=False)
        spec_bw = librosa.feature.spectral_bandwidth(y=y, sr=sr, n_fft=framesize, hop_length=framesize, center=False)
        rolloff = librosa.feature.spectral_rolloff(y=y, sr=sr, n_fft=framesize, hop_length=framesize, center=False)
        zcr = librosa.feature.zero_crossing_rate(y, frame_length=framesize, hop_length=framesize, center=False)
        mfcc = librosa.feature.mfcc(y=y, sr=sr, n_fft=framesize, hop_length=framesize, center=False)
            
        # D = np.array(D) #.reshape((-1, framesize/2 + 1))

        to_append = f'{np.mean(chroma_stft)} {np.mean(rmse)} {np.mean(spec_cent)} {np.mean(spec_bw)} {np.mean(rolloff)} {np.mean(zcr)}'
        for e in mfcc:
            to_append += f' {np.mean(e)}'
        # to_append += f' {g}'

        X = np.atleast_2d(to_append.split())
        # print(f'    X = {X}')
        X_s = scaler.transform(X)
        # print(f'    X_s = {X_s}')
        # y = encoder.fit_transform(genre_list)

        y_ = model.predict(X_s)

        # print(f'frame {blk_i} prediction {np.argmax(y_)}')
        # print(f'y_ = {np.argmax(y_)}')
        return np.argmax(y_)
    
    # start loop
    cnt = 0
    cnt_qu = 0
    g = 0
    g_s = np.zeros((1000,))
    try:
        while True:
            # read from the queue 
            while qu.qsize() > 0:
                # got one item
                qud = qu.get()
                # qud[1] = 0
                # print('qu {0}'.format(qud))
                # QUD[0,:] = np.array(qud)
                # QUD = np.roll(QUD, shift=1, axis=0)

                y_ = process_frame(cnt_qu, np.array(qud[1:]), model, scaler, framesize)
                g = 0.8 * g + 0.2 * y_
                g_s[0] = y_
                g_s = np.roll(g_s, 1)

                # print('g_s', g_s)
                # print(f'frame {blk_i} prediction {y_}')
                cnt_qu += 1
                
            cnt += 1

            # np.histogram(g_s, bins=range(6), density=True)
            
            # print loop status
            if cnt % 100 == 0:
                
                h_cnt, h_bins = np.histogram(g_s, bins=range(6), density=False)

                osc_msg_dmx[1] = int(h_cnt[0] * 1e-3 * 255)
                osc_msg_dmx[2] = int(h_cnt[1] * 1e-3 * 255)
                osc_msg_dmx[3] = int(h_cnt[2] * 1e-3 * 255)
                osc_msg_dmx[0] = 255
                osc_msg_dmx[10] = int(255)
                osc_msg_dmx[11] = int(h_cnt[3] * 1e-3 * 255)
                osc_msg_dmx[12] = int(h_cnt[4] * 1e-3 * 255)
                osc_msg_dmx[13] = 0
                print(osc_msg_dmx)
                oscsrv.server.send(osc_tgt_dmx, '/dmx/universe/1', osc_msg_dmx)
                
                print(f'cnt = {cnt}, cnt queue {cnt_qu}')
                print(f'stats = {pformat(oscsrv.cb_mfcc_stats)}')
                print(f'g = {g}')
                print(f'g_s = {np.histogram(g_s, bins=range(6), density=True)}')
                
            # sleep
            time.sleep(1e-3)
            
    except KeyboardInterrupt:
        print("key fin")
        oscsrv.isrunning = False
        oscsrv.st.join()
