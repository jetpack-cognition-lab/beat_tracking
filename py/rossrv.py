import re, threading, signal, time
import rospy

class ROSsrv(threading.Thread):
    def __init__(self, port=1234, queue=None):
        # thread init
        super(ROSsrv, self).__init__()
        # threading.Thread.__init__(self)
        
        self.server = None
        self.port = port
        self.queue = queue
        self.name = 'ROSsrv'
        
        self.name = str(self.__class__).split(".")[-1].replace("'>", "")
        # signal.signal(signal.SIGINT, self.shutdown_handler)

        rospy.init_node(self.name, anonymous=True)
        
        self.isrunning = True
        self.cnt_main = 0
        self.loop_time = 0.1
        
        self.sub = {}
        self.pub = {}
        
        # try:
        #     self.server = liblo.Server(self.port)
        #     print('rossrv started on port {0}'.format(self.port))
        #     self.isrunning = True
        # except liblo.ServerError as err:
        #     print(err)
        #     self.isrunning = False
        #     # sys.exit()
            
        # register method taking an int and a float
        # self.server.add_method("/mfcc", 'f' * 38, self.cb_mfcc)
        # self.server.add_method("/beat", 'f' * 3, self.cb_beat)

        # self.st = threading.Thread( target = self.run )
        # self.st.start()


        
    def add_method(self, path, types, callback):
        callback_name = 'cb_{0}'.format(re.sub('/', '_', path))
        print('rossrv')
        setattr(self, callback_name, callback)
        self.server.add_method(path, types, getattr(self, callback_name))

    # def cb_mfcc(self, path, args):
    #     # i, f = args
    #     # print("received message '%s' with arguments '%d' and '%f'" % (path, i, f))
    #     self.queue.put(args)
    #     # print('received args {0}'.format(args))

    # def cb_beat(self, path, args):
    #     print('got args = {0}'.format(args))
    #     self.queue.put(args)

    # def run(self):
    #     # loop and dispatch messages every 100ms
    #     while self.isrunning:
    #         self.server.recv(100)
    #     print('terminating')

    def add_pub(self, path, msgtype):
        self.pub[path] = rospy.Publisher('/{0}'.format(path), msgtype)
    
    def publish(self, publisher, msg):
        self.pub[publisher].publish(msg)
    
    def pub_sub(self):
        self.sub = {}
        self.pub = {}

    def srv(self):
        self.srv = {}

    def shutdown_handler(self, signum, frame):
        print(('rossrv.shutdown_handler called with signal', signum))
        rospy.signal_shutdown("ending")
        self.isrunning = False
        # self.join()
        # for sub in self.sub:
        #     self.sub.shutdown()
        #     # print sub.unregister()
        # for pub in self.pub:
        #     self.pub.shutdown()
        # sys.exit(0)
        
    def run(self):
        while self.isrunning:
            time.sleep(self.loop_time)
