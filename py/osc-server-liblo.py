#!/usr/bin/env python

from __future__ import print_function
import liblo, sys, time

import numpy as np
import matplotlib.pyplot as plt

# create server, listening on port 1234
try:
    server = liblo.Server(1234)
except liblo.ServerError as err:
    print(err)
    sys.exit()

# model
embed_len = 1
input_len = 38

W_in = np.random.uniform(-1e-2, 1e-2, (input_len, input_len))
x = np.zeros((input_len, 1))
y = np.zeros((input_len, 1))
e_ = [1,1]

plt.ion()
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(e_)
plt.draw()
# fig.show()
plt.show()

def mfcc_callback(path, args):
    # i, f = args
    # print("received message '%s' with arguments '%d' and '%f'" % (path, i, f))
    print('received args {0}'.format(args))

def mfcc_callback_learn(path, args):
    global W_in, x, y
    # i, f = args
    # print("received message '%s' with arguments '%d' and '%f'" % (path, i, f))
    # print('received args {0}'.format(args))
    print('received args type {0}, len {1}'.format(type(args), len(args)))
    x = np.array(args)
    e = x - y
    e_norm = np.linalg.norm(e, 2)
    e_.append(e_norm)
    print('|e| = {0}'.format(e_norm))
    # print('W_in = {0}'.format(W_in))
    # print('   e = {0}'.format(e))
    dW_in = (W_in.T * e).T
    # print('W_in = {0}'.format(W_in))
    print('dW_in = {0}, |dW_in| = {1}'.format(dW_in.shape, np.linalg.norm(dW_in, 2)))
    W_in += dW_in * 0.01
    print('|W_in| = {0}'.format(np.linalg.norm(W_in, 2)))
    y = np.dot(W_in, x)
    print('x = {0}, y = {1}'.format(x.shape, y.shape))
    plt.cla()
    ax.plot(np.clip(-2, 2, e_))
    plt.draw()
    # time.sleep(0.001)
    plt.pause(1e-3)

def foo_bar_callback(path, args):
    i, f = args
    print("received message '%s' with arguments '%d' and '%f'" % (path, i, f))

def foo_baz_callback(path, args, types, src, data):
    print("received message '%s'" % path)
    print("blob contains %d bytes, user data was '%s'" % (len(args[0]), data))

def fallback(path, args, types, src):
    print("got unknown message '%s' from '%s'" % (path, src.url))
    for a, t in zip(args, types):
        print("argument of type '%s': %s" % (t, a))

# register method taking an int and a float
server.add_method("/mfcc", 'ffffffffffffffffffffffffffffffffffffff', mfcc_callback_learn)

# register method taking an int and a float
server.add_method("/foo/bar", 'if', foo_bar_callback)

# register method taking a blob, and passing user data to the callback
server.add_method("/foo/baz", 'b', foo_baz_callback, "blah")

# register a fallback for unhandled messages
server.add_method(None, None, fallback)

# loop and dispatch messages every 100ms
while True:
    server.recv(100)
