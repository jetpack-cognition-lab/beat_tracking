"""Beat tracking in real time with librosa, latest version

Oswald Berthold, Paul Azoulai, 2018

Usage:
python3 beat_tracking_rt_librosa.py

Internally uses sounddevice to record live input in one thread,
process that with librosa and store the current tempo estimate in
sleep times in shared memory. In another thread we just loop, play a
click sound using custom alsaplay
"""
import librosa
import sounddevice as sd
import numpy as np
import time
import threading
import mido

print(sd.query_devices())


""" communication variables """

tempo = [60]

def compute_tempo(tempo) :
    fs = 44100
    duration = 5
    while True :
        now = time.time()
        myrecording = sd.rec(int(duration * fs), samplerate=fs, channels=1, device=2, blocking=True)
        # print(type(myrecording))
        # print(myrecording.shape)
        # myrecording = myrecording[:]
        L = myrecording.tolist()
        M = np.asarray([elt[0] for elt in L])
        print(L)
        tempo_bpm, beats = librosa.beat.beat_track(y=M, sr=fs)
        print(tempo_bpm)
        try :
            to_wait = 60/tempo_bpm
            tempo[0] = to_wait
            # return to_wait
        except :
            tempo[0] = 60

def play_beat(tempo) :
    msg = mido.Message('note_on', note=60)
    port = mido.open_output('abletone')
    while True :
        port.send(msg)
        time.sleep(tempo[0])
        
t1 = threading.Thread(target=compute_tempo, args = (tempo,))
t2 = threading.Thread(target=play_beat, args = (tempo,))

threads = [t1, t2]

for t in threads :
    t.start()

for t in threads :
    t.join()
    


    
