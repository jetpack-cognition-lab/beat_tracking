"""experiment: clock sync by incremental phase manipulation

oswald berthold, 10/2019
"""

import threading, time, logging, argparse
import random, queue
import numpy as np

# import pygame.mixer
import simpleaudio as sa

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

class EXCL(threading.Thread):
    def __init__(self, *args, **kwargs):
        self.qu = None
        self.mu = 0.5
        for param in ['qu', 'mu']:
            if param in kwargs:
                setattr(self, param, kwargs[param])
                del kwargs[param]
            
        super(EXCL, self).__init__(*args, **kwargs)
        self.i = 0
        self.time_last = time.time()
        self.time_cur = time.time()
        self.period_len = random.gauss(self.mu, 0.033)
        # self.snd = pygame.mixer.Sound('click1.wav')
        self.snd = sa.WaveObject.from_wave_file('click1.wav')
        logger.info('{0} init period_len = {1}'.format(self.__class__.__name__, self.period_len))

    def run(self):
        logger.info('{0} starting'.format(self.__class__.__name__))
        while True:
            self.time_cur = time.time()
            self.snd.play()
            logger.info('{0} tick {1} @{2}'.format(self.__class__.__name__, self.i, self.period_len))
            if self.qu is not None:
                self.qu.put_nowait(self.time_cur)
            self.i += 1
            # logger.info('    sleeptime = {0}'.format(sleeptime))
            if self.i % 16 == 0:
                self.period_len = random.gauss(self.mu, 0.2)
                logger.info('{0} new period_len = {1}'.format(self.__class__.__name__, self.period_len))

            time.sleep(self.period_len)
            # sleeptime = self.period_len - (time.time() - self.time_cur)
            # time.sleep(sleeptime)

class SYCL(threading.Thread):
    def __init__(self, *args, **kwargs):
        self.qu = None
        if 'qu' in kwargs:
            self.qu = qu
            del kwargs['qu']
            
        super(SYCL, self).__init__(*args, **kwargs)
        self.i = 0
        self.time_last = time.time()
        self.time_cur = time.time()
        self.period_len = 0.5
        self.period_dphase = 0
        self.time_delta_1_ = np.zeros((10,))
        self.time_delta_2_ = np.zeros((10,))
        self.evt_last = 0
        # self.snd = pygame.mixer.Sound('click2.wav')
        self.snd = sa.WaveObject.from_wave_file('click2.wav')
        logger.info('{0} init period_len = {1}'.format(self.__class__.__name__, self.period_len))
        logger.info('   {0}, {1}'.format(self.time_delta_1_, self.time_delta_2_))

    def run(self):
        logger.info('{0} starting'.format(self.__class__.__name__))
        while True:
            # evt = time.time()
            evt = 0
            dphase_local = 0
            self.snd.play()
            logger.info('{0} tick {1} @{2}'.format(self.__class__.__name__, self.i, self.period_len))
            self.time_cur = time.time()
            if self.qu is not None:
                while self.qu.qsize() > 0:
                    evt = self.qu.get()
                    
            if evt != 0:
                time_delta_1 = evt - self.time_cur
                time_delta_2 = evt - self.time_last
                logger.info('{0} time_delta_1 = {1}, time_delta_2 = {2}'.format(self.__class__.__name__, time_delta_1, time_delta_2))

                time_delta_1_n = time_delta_1 / self.period_len
                time_delta_2_n = time_delta_2 / self.period_len
                # logger.info('   {0}, {1}'.format(time_delta_1_n, time_delta_2_n))

                period_len_ext_1 = abs(time_delta_1) + abs(time_delta_2)
                period_len_ext_2 = evt - self.evt_last
                # logger.info('   period_len_ext_1 = {0}, period_len_ext_2 = {1}'.format(period_len_ext_1, period_len_ext_2))

                dphase_local = 0.5 * time_delta_1
                if period_len_ext_2 < (self.period_len * 1.5):
                    dphase_global = -0.5 * (self.period_len - period_len_ext_2)
                    self.period_len += dphase_global
                
                self.time_delta_1_[0] = time_delta_1_n
                self.time_delta_1_ = np.roll(self.time_delta_1_, 1)
                self.time_delta_2_[0] = time_delta_2_n
                self.time_delta_2_ = np.roll(self.time_delta_2_, 1)
                # logger.info('   {0}, {1}'.format(self.time_delta_1_, self.time_delta_2_))
                
                # dphase_1 = self.time_delta_1_[1] - self.time_delta_1_[2]
                # logger.info('evt = {0}, time_delta = {1:.2f}/{2:.2f}'.format(evt,
                #                                                          self.time_delta_1_[1],
                #                                                          dphase_1))
                # dphase_2 = self.time_delta_2_[1] - self.time_delta_2_[2]
                # logger.info('evt = {0}, time_delta = {1:.2f}/{2:.2f}'.format(evt,
                #                                                          self.time_delta_2_[1],
                #                                                          dphase_2))
                self.evt_last = evt
                
            self.time_last = self.time_cur

            # dsleep = self.period_len
            # period_dphase = 0
            # if evt != 0: # and self.time_delta_2_[1] > 0:
            #     period_dphase = max(0, 0.1 * dphase_1) # 0.5 * (dphase_1 * self.period_len)
            #     # dsleep = 1 - abs(self.time_delta_1_[1])
            # estimate:
            # - where in the phase
            # - moving which direction
            
            self.i += 1
            # time.sleep(dsleep)
            # self.period_len += period_dphase
            
            # sleeptime = self.period_len - (time.time() - self.time_cur)
            time.sleep(self.period_len + dphase_local)
            
    def run2(self):
        logger.info('{0} starting'.format(self.__class__.__name__))
        while True:
            # evt = time.time()
            evt = 0
            dphase_1 = 0
            dphase_2 = 0
            logger.info('{0} ticking {1}'.format(self.__class__.__name__, self.i))
            self.time_cur = time.time()
            if self.qu is not None:
                while self.qu.qsize() > 0:
                    evt = self.qu.get()
            if evt != 0:
                time_delta_1 = evt - self.time_cur
                time_delta_2 = evt - self.time_last

                time_delta_1_ph = time_delta_1 / self.period_len
                time_delta_2_ph = time_delta_2 / self.period_len
                logger.info('   {0}, {1}'.format(time_delta_1_ph, time_delta_2_ph))
                self.time_delta_1_[0] = time_delta_1_ph
                self.time_delta_1_ = np.roll(self.time_delta_1_, 1)
                self.time_delta_2_[0] = time_delta_2_ph
                self.time_delta_2_ = np.roll(self.time_delta_2_, 1)
                # logger.info('   {0}, {1}'.format(self.time_delta_1_, self.time_delta_2_))

                dphase_1 = self.time_delta_1_[1] - self.time_delta_1_[2]
                logger.info('evt = {0}, time_delta = {1:.2f}/{2:.2f}'.format(evt,
                                                                         self.time_delta_1_[1],
                                                                         dphase_1))
                dphase_2 = self.time_delta_2_[1] - self.time_delta_2_[2]
                logger.info('evt = {0}, time_delta = {1:.2f}/{2:.2f}'.format(evt,
                                                                         self.time_delta_2_[1],
                                                                         dphase_2))
            self.time_last = self.time_cur

            dsleep = self.period_len
            period_dphase = 0
            if evt != 0: # and self.time_delta_2_[1] > 0:
                period_dphase = max(0, 0.1 * dphase_1) # 0.5 * (dphase_1 * self.period_len)
                # dsleep = 1 - abs(self.time_delta_1_[1])
            # estimate:
            # - where in the phase
            # - moving which direction
            
            self.i += 1
            # time.sleep(dsleep)
            self.period_len += period_dphase
            time.sleep(self.period_len)

if __name__ == '__main__':
    # arg parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mu', type=float, default=0.5, help='Mean for sampling period of random external clock [0.5]')
    args = parser.parse_args()
    
    # get arguments
    qu = queue.Queue(maxsize=2)

    # pygame.mixer.init()
    
    # create syncing clock CLSY
    sycl = SYCL(qu=qu, daemon=True)
    # create external clock EXCL
    excl = EXCL(qu=qu, daemon=True, mu=args.mu)

    # start SYCL thread
    sycl.start()
    # start EXCL thread
    excl.start()

    while True:
        time.sleep(1)
