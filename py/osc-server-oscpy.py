from oscpy.server import OSCThreadServer
from time import sleep

osc = OSCThreadServer()
sock = osc.listen(address='0.0.0.0', port=1234, default=True)

@osc.address(b'/mfcc')
def callback(*values):
    print("got values: {}".format(values))

sleep(10)
osc.stop()
