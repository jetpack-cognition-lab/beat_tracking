"""osc2sensorimotor

bridge between OSC and libsensorimotor
"""
from __future__ import print_function
import liblo, sys, time, queue, pickle, argparse
import threading

from src.sensorimotor import Sensorimotor
from time import sleep

from oscsrv import OSCsrv

def main(args):
    # create queue
    qu = queue.Queue(maxsize=10)
    
    # create server, listening on port 1234
    oscsrv = OSCsrv(port=args.port, queue=qu)

    # fix pd
    target = liblo.Address(1337)
    liblo.send(target, "/reconnect", 'bang')

    # motors = Sensorimotor(number_of_motors = 3, verbose = False)
    motors = Sensorimotor(number_of_motors = 3, verbose = False, update_rate_Hz = 300)

    try:
        # checking for motors
        N = motors.ping()
        print("Found {0} sensorimotors.".format(N))
        sleep(1.0)

        #TODO: set this according to your supply voltage and desired max. motor speed
        motors.set_voltage_limit([0.8, 0.8, 0.8])

        # starting motorcord
        motors.start()

        # beat = tst0
    except (Exception):
        print('failed to start motors')
        pass
        

    beats = [[0.6, 0.0, 0.0], [0.0, 0.6, 0.0], [0.0, 0.0, 0.6]]

    cnt = 0
    try:
        while True:
            # print('main loop cnt = {0}'.format(cnt))
            qud = None
            while qu.qsize() > 0:
                qud = qu.get()
                # qud[0] = 0
                # print('qu {0}'.format(qud))
                # QUD[0,:] = np.array(qud)
                # QUD = np.roll(QUD, shift=1, axis=0)

                print('qud = {0}'.format(qud))
            # USER CODE HERE BEGIN
            # motors.apply_impulse(b[0])
            # b = beats[cnt%3]
            if qud is not None:
                b = qud
                print('beat b = {0}'.format(b))
                motors.apply_impulse(b)
            
            cnt += 1
            # print('cnt={0}'.format(cnt))
            
            if cnt % 1000 == 0:
                # gng.stop_training()
                
                # f = open('gng.bin', 'wb')
                # pickle.dump(gng, f)
                # f.close()
                print('cnt = {0}'.format(cnt))
                pass
                
            time.sleep(1e-3)
            
    except (KeyboardInterrupt, SystemExit):
        print("key fin")
        oscsrv.isrunning = False
        # stopping motor cord
        print("\rAborted, stopping motors")
        motors.stop()
        # plt.ioff()
        # gng.stop_training()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, help="OSC server port [1234]", default=1234)
    args = parser.parse_args()
    main(args)
