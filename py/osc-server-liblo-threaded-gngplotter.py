from __future__ import print_function
import liblo, sys, time, queue, pickle
import threading

import numpy as np
import matplotlib.pyplot as plt
import mdp
from sklearn.decomposition import PCA, IncrementalPCA, KernelPCA

class OSCsrv(object):
    def __init__(self, port=1234, queue=None):
        self.server = None
        self.port = port
        self.queue = queue
        try:
            self.server = liblo.Server(self.port)
            self.isrunning = True
        except liblo.ServerError as err:
            print(err)
            self.isrunning = False
            # sys.exit()
            
        # register method taking an int and a float
        self.server.add_method("/mfcc", 'f' * 38, self.cb_mfcc)

        self.st = threading.Thread( target = self.run )
        self.st.start()

    def cb_mfcc(self, path, args):
        # i, f = args
        # print("received message '%s' with arguments '%d' and '%f'" % (path, i, f))
        self.queue.put(args)
        # print('received args {0}'.format(args))

    def run(self):
        # loop and dispatch messages every 100ms
        while self.isrunning:
            self.server.recv(100)
        print('terminating')

class GNGplotter():
    def __init__(self, gng):
        # f = open("RLCamctrlGNG-2012-11-08-150350-ok.bin", "rb")
        # f = open("RLCamctrlGNG-2012-11-12-145759.bin", "rb")
        # f = open("RLCamctrlGNG-2012-11-12-161544.bin", "rb")
        # f = open("RLCamctrlGNG-2012-11-13-145512.bin", "rb")
        # unp = pickle.Unpickler(f)
        self.gng = gng # unp.load()
        # del unp
        # f.close()

    def plotLearning(self):
        self.m = np.genfromtxt("RLCamctrlGNG-2012-11-15-161005-lrn.dat")
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.ax.plot(
            np.multiply(
                self.m,
                np.array((100, 100, 1, 1))
                ) - np.array((50, 50, 0, 0)))
        self.ax.set_aspect(0.6)
        self.ax.legend(("a(t)", "a-bar(t)", "R(t)", "R-bar(t)"), loc=0)
        self.fig.savefig("blub.png")
        plt.show()
        
    def plotit(self, fig, ax, tf):
        self.fig = fig 
        self.ax = ax
        plt.draw()
        plt.show()

        g_dim = self.gng.input_dim
        g_nodes = self.gng.get_nodes_position()
        
        X_kpca = tf.fit_transform(g_nodes)
        X_back = tf.inverse_transform(X_kpca)
                
        # print("g_nodes", g_nodes)
        # print("g_nodes", g_nodes[:,0])
        # print("g_nodes", g_nodes[:,3])
        # self.g_nodes_x = g_nodes[:,0]
        # self.g_nodes_y = g_nodes[:,3]
        # self.g_nodes_x = lle_projected_data[:,0]
        # self.g_nodes_y = lle_projected_data[:,1]
        self.g_nodes_x = X_kpca[:,0]
        self.g_nodes_y = X_kpca[:,1]
        
        self.plt_nodes, = self.ax.plot((self.g_nodes_x+1)*128,
                                       (self.g_nodes_y+1)*128,
                                       "o", ms=10)

        for edge in self.gng.graph.edges:
            # print('edge = {0}'.format(edge))
            # n1 = np.array([edge.head.data.pos[0], 
            #                edge.tail.data.pos[0]])
            # n2 = np.array([edge.head.data.pos[3], 
            #                edge.tail.data.pos[3]])
            n1 = tf.transform(np.atleast_2d(edge.head.data.pos))
            n2 = tf.transform(np.atleast_2d(edge.tail.data.pos))
            n3 = np.vstack((n1, n2))
            # n1 = np.array([edge.head.data.pos[0], 
            #                edge.tail.data.pos[0]])
            # n2 = np.array([edge.head.data.pos[3], 
            #                edge.tail.data.pos[3]])
            # print('n1, n2, n3', n1, n2, n3)
            # self.ax.plot((n1+1)*128, (n2+1)*128, "k-", lw=5)
            self.ax.plot((n3[:,0]+1)*128, (n3[:,1]+1)*128, "k-", lw=1, alpha=0.5)
            # self.ax.plot()

        self.ax.set_ylim(0, 200)
        self.ax.set_xlabel("Exposure")
        self.ax.set_ylabel("Mean Pixel Intensity")
        self.ax.set_aspect(0.5)
        # print("plt_nodes", self.plt_nodes)
        # self.fig.savefig("blub.png")
        plt.draw()
        # plt.show()
        plt.pause(1e-3)
        # return kpca
        return fig, ax
        
if __name__ == '__main__':
    qu = queue.Queue(maxsize=10)
    
    # create server, listening on port 1234
    oscsrv = OSCsrv(port=1234, queue=qu)

    # fix pd
    target = liblo.Address(1337)
    liblo.send(target, "/reconnect", 'bang')

    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(1, 1, 1)
    
    # ax.plot(e_)
    # ax.set_aspect(4)
    plt.draw()

    # fig.show()
    plt.show()

    QUD = np.zeros((500, 38))

    gng = mdp.nodes.GrowingNeuralGasNode(max_nodes=75)
    gngp = GNGplotter(gng)
    
    # k = 5
    # lle_projected_data = mdp.nodes.LLENode(k, output_dim=2)(g_nodes)

    kpca = KernelPCA(kernel="rbf", n_components=2, fit_inverse_transform=True, gamma=10)

    cnt = 0
    try:
        while True:
            # print('main loop cnt = {0}'.format(cnt))
            while qu.qsize() > 0:
                qud = qu.get()
                qud[0] = 0
                # print('qu {0}'.format(qud))
                QUD[0,:] = np.array(qud)
                QUD = np.roll(QUD, shift=1, axis=0)

                if gng.is_training():
                    gng.train(np.atleast_2d(qud))
                else:
                    # gqud = gng.execute(np.atleast_2d(qud))
                    nqud = gng.nearest_neighbor(np.atleast_2d(qud))
                    # print('nqud = {0}'.format(nqud))
                    gqud = np.atleast_2d(nqud[0][0].data.pos)
                    # print('qud[{1}] = {0}'.format(qud, qu.qsize()))
                    # print('gqud[{1}] = {0}'.format(gqud, qu.qsize()))
                    
            # plt.cla()
            ax.clear()
            ax.imshow(QUD.T)
            ax.set_aspect(4)
            plt.draw()
            # time.sleep(0.001)
            plt.pause(1e-3)

            if not gng.is_training():
                ax2.clear()
                gngp.plotit(fig2, ax2, kpca)

                gqud_kpca = kpca.transform(gqud)
                ax2.plot((gqud_kpca[0,0]+1)*128, (gqud_kpca[0,1]+1)*128, 'o', ms=20)
                plt.draw()
                plt.pause(1e-3)
            
            cnt += 1
            print('cnt={0}'.format(cnt))
            
            if cnt == 1000:
                gng.stop_training()
                
                f = open('gng.bin', 'wb')
                pickle.dump(gng, f)
                f.close()
                
            time.sleep(1e-2)
            
    except KeyboardInterrupt:
        print("key fin")
        oscsrv.isrunning = False
        plt.ioff()
        # gng.stop_training()
