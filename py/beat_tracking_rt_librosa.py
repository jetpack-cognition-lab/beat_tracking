"""Beat tracking in real time with librosa, test version

Oswald Berthold, Paul Azoulai, 2018

Usage:
python3 beat_tracking_rt_librosa.py

Internally uses sounddevice to record live input in one thread,
process that with librosa and store the current tempo estimate in
sleep times in shared memory. In another thread we just loop, play a
click sound using custom alsaplay
"""
import os, time, threading
now = time.time()
print('importing librosa')
import librosa
print('import librosa took {0}'.format(time.time()-now))

now = time.time()
import sounddevice as sd
print('import sounddevice took {0}'.format(time.time()-now))

now = time.time()
import numpy as np
print('import numpy took {0}'.format(time.time()-now))

# import mido
# import socket
# import os

# print(sd.query_devices())

import wave
import alsaaudio
from alsaplay import init, play

""" communication variables """

tempo = [0.5]
# click = librosa.load
# sd.default.device = 'USB PnP Sound Device'
# click, sr = librosa.load('click1.wav', offset=0, duration=10)

# device = 'default'
device = 'plughw:1'

# opts, args = getopt.getopt(sys.argv[1:], 'd:')
# for o, a in opts:
#     if o == '-d':
#         device = a
        
#     if not args:
#         usage()
        
# f = wave.open(args[0], 'rb')
f = wave.open('click1.wav', 'rb')
device = alsaaudio.PCM(device=device)
periodsize = init(device, f)

# from pydub import AudioSegment
# import pydub.playback as pb
# song = AudioSegment.from_wav("click1.wav")
# pb.play(song)

# TODO
# - use periodsize recording thread to write into larger ring buffer / delay line
# - use sd.playrec to record chunk of audio into buffer, play back chunck of audio if needed

def compute_tempo(tempo, main_running=True) :
    fs = 44100
    # fs = 22050
    duration = 3
    while main_running:
        now = time.time()
        # print('sd.status', sd.get_status())
        sd.stop()
        myrecording = sd.rec(int(duration * fs), samplerate=fs, channels=1, device=2, blocking=True)
        # myrecording = np.random.uniform(-1, 1, (1, int(duration * fs)))
        L = myrecording.tolist()
        M = np.asarray([elt[0] for elt in L])
        # print(M)
        tempo_bpm, beats = librosa.beat.beat_track(y=M, sr=fs)
        print(tempo_bpm)
        to_wait = 60/tempo_bpm
        tempo[0] = to_wait
        # tempo[0] = 0.5
        # time.sleep(3)

def play_beat(tempo, device, buf, main_running=True) :
    last = time.time()
    sr=44100
    while main_running:
        elapsed = time.time() - last
        overelapsed = max(0, (elapsed-tempo[0]))
        sleeptime = tempo[0]-overelapsed
        sleeptime_ = max(0.001, sleeptime)
        print('sleep = {1}, over = {0}'.format(overelapsed, sleeptime))
        time.sleep(sleeptime_)
        last = time.time()
        if overelapsed <= tempo[0]:
            # sd.stop()
            # print('sd.status', sd.get_status())
            # # sounddevice play (change to sd.playrec()
            # sd.play(click, samplerate=sr)#, device=2)
            # alsaplay play
            play(device, buf, periodsize)
            buf.rewind()
        else:
            pass
        # print("tip")
        # print('elapsed {0}'.format(elapsed))
        # time.sleep(tempo[0])
        # print("tap")
        # os.system("echo -ne '\\007'")

t1 = threading.Thread(target=compute_tempo, args = (tempo,))
t2 = threading.Thread(target=play_beat, args = (tempo, device, f))
# # t3 = threading.Thread(target=enter_tempo, args = (tempo,))
# 
# 
# 
threads = [t1, t2]

for t in threads :
    t.daemon = True
    t.start()

# for t in threads :
#     t.join()


# print('sr', sr)
# sr=44100
# sd.play(click, samplerate=sr, device=5)
# play(device, f)
# f.rewind()
        
# play_beat(tempo, device=device, buf=f)

while True:
    time.sleep(1)

f.close()


# def enter_tempo(tempo) :
#     while True :
#         tempo[0] = input("-->")
