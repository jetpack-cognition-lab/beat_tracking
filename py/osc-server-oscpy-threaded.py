from oscpy.server import OSCThreadServer
# from time import sleep
import time

class OSCsrv(object):
    def __init__(self):
        self.osc = OSCThreadServer()
        self.sock = self.osc.listen(address='0.0.0.0', port=1234, default=True)

    @self.osc.address(b'/mfcc')
    def callback(self, *values):
        print("got values: {}".format(values))

if __name__ == '__main__':

    oscsrv = OSCsrv()
    
    cnt = 0
    try:
        while True:
            print('main loop cnt = {0}'.format(cnt))
            cnt += 1
            time.sleep(1.0)
    except KeyboardInterrupt:
        print("key fin")
        oscsrv.osc.stop()
