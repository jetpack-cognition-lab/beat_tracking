"""meshhub

heterogenous channel and message hub

pub/sub server for message distribution across different phy,
transport and protocol layers

mainly: osc, ros, zmq
"""
import argparse, queue, time, signal, sys
from functools import partial

from oscsrv import OSCsrv
from rossrv import ROSsrv

from std_msgs.msg import Int32MultiArray

def shutdown_handler(threads, signum, frame):
    print('meshhub.shutdown_handler called with signal {0}'.format(signum))
    for thr in threads:
        print('    terminating thread {0}'.format(thr))
        thr.isrunning = False
        if hasattr(thr, 'st'):
            thr.st.join()
        else:
            thr.join()
    # rospy.signal_shutdown("ending")
    # self.isrunning = False
    sys.exit(0)

def cb_hexagon_sensors(qu, path, args, types, target, unk):
    # def cb_hexagon_motors(*args, **kwargs):
    # i, f = args
    # print('args = {0}, kwargs = {1}'.format(args, kwargs))
    # print('qu = {0}'.format(qu))
    print("cb_hexagon_sensors message {0} with arguments {1}".format(path, args))
    qu.put((path, args))
    # print('received args {0}'.format(args))

def cb_queue_osc(qud, srv):
    print('cb_queue_osc qud = {0}'.format(qud))
    # msg = Int32MultiArray(qud[1])
    msg = Int32MultiArray()
    msg.data = qud[1]
    print('cb_queue_osc msg = {0}'.format(msg))
    srv.publish('hexagon_sensors', msg)

def cb_queue_ros(qud):
    print('cb_queue_ros qud = {0}'.format(qud))

def main(args):
    # channel queues
    qus = {
        'osc': queue.Queue(maxsize=10),
        'ros': queue.Queue(maxsize=10),
    }
    # start protocol sockets
    # osc
    oscsrv = OSCsrv(port=args.port, queue=qus['osc'])
    
    # [7, 133, 130, 129, 128, 0, 0, 135, 132, 1, 137]
    oscsrv.add_method(
        path="/hexagon_sensors",
        # types='f'*number_of_motors,
        types='iiiiiiiiiiii',
        callback=partial(cb_hexagon_sensors, qus['osc'])
    )
    
    # ros
    rossrv = ROSsrv(port=None, queue=qus['ros'])
    rossrv.add_pub('hexagon_sensors', Int32MultiArray)
    # # learning control
    # self.sub["ctrl_eta"]      = rospy.Subscriber("/learner/ctrl/eta", Float32, self.sub_cb_ctrl)
    rossrv.start()

    threads = [oscsrv, rossrv]
    signal.signal(signal.SIGINT, partial(shutdown_handler, threads))

    # start main loop
    cnt = 0
    while True:
        # print('tick')
        # print('main loop cnt = {0}'.format(cnt))
        qu = qus['osc']
        qud = None
        while qu.qsize() > 0:
            qud = qu.get()
            # qud[0] = 0
            # print('qu {0}'.format(qud))
            # QUD[0,:] = np.array(qud)
            # QUD = np.roll(QUD, shift=1, axis=0)
            
            # print('qud = {0}'.format(qud))
            
            # USER CODE HERE BEGIN
            # motors.apply_impulse(b[0])
            # b = beats[cnt%3]
            if qud is not None:
                # loopfunc_hexagon(qud[1], cord)
                cb_queue_osc(qud, rossrv)

        qu = qus['ros']
        while qu.qsize() > 0:
            qud = qu.get()
            if qud is not None:
                cb_queue_ros(qud, oscsrv)
                
        cnt += 1
        if cnt % 1000 == 0:
            print('meshhub loop cnt = {0}'.format(cnt))
        time.sleep(1e-3)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, help="Port number [1237]", default=1237)
    args = parser.parse_args()
    main(args)
