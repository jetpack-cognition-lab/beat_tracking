
################################################
# on raspy with pisound shield

# start jack daemon
jackd -d alsa -d hw:1 -r 48000 -rt

# connect moc input
for i in 0 1 ; do jack_connect moc:output${i} pure_data:input${i} ; done

# connect midi
aconnect 129:1 20:0


###############################################
# tools
vmpk
fluid synth
timidity

aplaymidi
arecordmidi
aconnect
