/*
 * HOWTO write an External for Pure data
 * (c) 2001-2006 IOhannes m zm�lnig zmoelnig[AT]iem.at
 *
 * this is the source-code for the first example in the HOWTO
 * it creates an object that prints "Hello world!" whenever it 
 * gets banged.
 *
 * for legal issues please see the file LICENSE.txt
 */

/*
  *syncclock*: adaptive clock synchronizing to external clock
               given a stochastic input _clock_ signal
   *clockify*: turn any stochastic event stream into a clocklike
               stream with a minimum ioi (just miniioi, refractory)
 *eventmerge*: merge a bundle of events close in time into a
               single event

 *arguments*:
 1. perferred freq
 2. clockify on/off
 3. eta_freq
 4. eta_phi

 *outlets*
 1. clockbang
 2. freq
 3. freq err
 4. phase
 5. phase err

 *test signals / input constraints*
 - regular clock pulse input
 - gaussian ioi clock pulse input
 - random clock pulse input
 - partial double time rhythmic input
 - partial triple and double time rhythmic input
 */

/**
 * include the interface to Pd 
 */
#include "m_pd.h"
#include "math.h"

/**
 * define a new "class" 
 */
static t_class *syncclockmulti_class;

/**
 * this is the dataspace of our new object
 * we don't need to store anything,
 * however the first (and only) entry in this struct
 * is mandatory and of type "t_object"
 */
typedef struct _syncclockmulti {
  t_object  x_obj; // object reference
  t_clock *x_clock; // main clock
  double x_ticktime; // ticktime for phase check
  double f_phi; // current clock phase
  double f_phi_err; // current clock phase error
  float f_eta_phi; // clock phase update rate
  double f_dphi; // current clock phase change
  double f_freq; // current clock frequency
  double f_freq_err; // current clock frequency error
  float f_eta_freq; // clock frequency update rate
  double f_period; // current clock period
  float f_period_in; // current clock period
  double f_freq_pref; // local preferred clock frequency
  double f_period_pref; // local preferred current clock period
  t_int do_update;
  
  // bangtime_last
  double time_of_last_bang_sec;
  // bangtime_cur
  double time_of_cur_bang_sec;
  // bangtime_delta
  double bang_to_bang_delta;

  // parameter inlets
  t_inlet *inlet_eta_freq;
  t_inlet *inlet_eta_phi;
  t_inlet *inlet_period;
  
  t_outlet *outlet_clock;
  t_outlet *outlet_freq, *outlet_freq_err;
  t_outlet *outlet_phi, *outlet_phi_err;
} t_syncclockmulti;

/**
 * from https://stackoverflow.com/questions/4633177/c-how-to-wrap-a-float-to-the-interval-pi-pi
 * change to `float/fmodf` or `long double/fmodl` or `int/%` as appropriate
 */

/* wrap x -> [0,max) */
double wrapMax(double x, double max)
{
    /* integer math: `(max + x % max) % max` */
    return fmod(max + fmod(x, max), max);
}
/* wrap x -> [min,max) */
double wrapMinMax(double x, double min, double max)
{
    return min + wrapMax(x - min, max - min);
}

/**
 * this method is called whenever a "bang" is sent to the object
 * the name of this function is arbitrary and is registered to Pd in the 
 * syncclockmulti_setup() routine
 */
void syncclockmulti_bang(t_syncclockmulti *x)
{
  /*
   * post() is Pd's version of printf()
   * the string (which can be formatted like with printf()) will be
   * output to wherever Pd thinks it has too (pd's console, the stderr...)
   * it automatically adds a newline at the end of the string
   */
  post("syncclockmulti_bang, phi=%f", x->f_phi);
  
  // get current time in milliseconds
  x->time_of_cur_bang_sec = sys_getrealtime();
  x->do_update = 1;
  
  // calculate beat period / period estimate
  x->bang_to_bang_delta = ((double) (x->time_of_cur_bang_sec - x->time_of_last_bang_sec))/1.0;

  if (x->bang_to_bang_delta >= (x->f_period_pref * 0.9)) {
    // store time since last bang
    x->time_of_last_bang_sec = x->time_of_cur_bang_sec;
  }
  
  /* else { */
  /*   post("syncclockmulti_bang, discarded bang_to_bang_delta = %f", x->bang_to_bang_delta); */
  /*   x->do_update = 0; */
  /*   return; */
  /* } */

  /**
   * clockify
   * - improve freq measurement
   *  - event periodicity
   * - improve phase measurement
   *  - weed out spurious events
   *  - only allow events in a window of attention around a plausible phase
   *  - use ioi-histogram
   *  - use internal confidence
   */
  
  if (x->bang_to_bang_delta < (x->f_period_pref * 0.9) || x->bang_to_bang_delta > (x->f_period_pref * 1.1)) {
    // discard and return
    post("syncclockmulti_bang, discarded bang_to_bang_delta = %f", x->bang_to_bang_delta);
    x->do_update = 0;
    // return;
  }

  // expand bang-to-bang-delta into ioi histogram
  
  // update freq prediction with error from freq measurement
  // x->f_freq_err = x->f_freq_pref - (1.0/x->bang_to_bang_delta);
  x->f_freq_err = x->f_freq_pref - (1.0/x->f_period_in);

  if (fabs(x->f_freq_err / x->f_freq_pref) > 0.2) {
    post("syncclockmulti_bang, discarded freq_err = %f", x->f_freq_err);
    x->do_update = 0;
    // return;
  }

  // instantaneous freq error
  // x->f_freq_err = x->f_freq - (1.0/x->bang_to_bang_delta);
  x->f_freq_err = x->f_freq - (1.0/x->f_period_in);
  
  if(x->do_update > 0) {

    // instantaneous phase error
    if (x->f_phi < 0.5) {
      x->f_phi_err = x->f_phi;
    }
    else {
      x->f_phi_err = x->f_phi - 1.0;
    }

    // update freq
    x->f_freq = x->f_freq - x->f_eta_freq * x->f_freq_err;
    post("syncclockmulti_bang, f_freq=%f", x->f_freq);
    // update phase increment from freq
    x->f_dphi = x->f_freq/1000.0;
    post("syncclockmulti_bang, f_freq=%f", x->f_dphi);
    // update period
    x->f_period = 1.0/x->f_freq;
    
    // update instantaneous phase
    x->f_phi = x->f_phi - (x->f_eta_phi * x->f_phi_err);
    
  }
  else {
    x->f_phi_err = 0;
  }
  /* // update instantaneous phase */
  /* x->f_phi_err = x->f_phi - 0; */
  /* if (x->f_phi_err < 0.5) { */
  /*   x->f_phi = x->f_phi - (x->f_eta_phi * x->f_phi_err); */
  /* } */
  /* else { */
  /*   x->f_phi = x->f_phi + (x->f_eta_phi * (1.0 - x->f_phi)); */
  /* } */
  
  outlet_float(x->outlet_freq, x->f_freq);
  outlet_float(x->outlet_freq_err, x->f_freq_err);
  outlet_float(x->outlet_phi_err, x->f_phi_err);
  
  post("syncclockmulti_bang, bperiod=%f", x->bang_to_bang_delta);
  // clock_delay(x->x_clock, x->x_ticktime);
}

/* void syncclockmulti_dsp(t_syncclockmulti *x, t_signal **sp) */
/* { */
/*   post("syncclockmulti_dsp"); */
/*   /\* dsp_add(syncclockmulti_perform, 5, x, *\/ */
/*   /\*         sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[0]->s_n); *\/ */
/* } */

static void syncclockmulti_tick(t_syncclockmulti *x)
{
  // post("syncclockmulti tick");
  clock_delay(x->x_clock, x->x_ticktime);
  // outlet_bang(x->x_obj.ob_outlet);
  
  x->f_phi = x->f_phi + x->f_dphi;
  
  if(x->f_phi >= 1.0) {
    x->f_phi = wrapMinMax(x->f_phi, 0.f, 1.f);
    // outlet_bang(x->x_obj.ob_outlet);
    outlet_bang(x->outlet_clock);
    // x->f_phi = x->f_phi - 1.0;
  }

  outlet_float(x->outlet_phi, x->f_phi);
}

/**
 * this is the "constructor" of the class
 * this method is called whenever a new object of this class is created
 * the name of this function is arbitrary and is registered to Pd in the 
 * syncclockmulti_setup() routine
 */
void *syncclockmulti_new(t_symbol *s, int argc, t_atom *argv)
{
  post("syncclockmulti_new s = %s", s);
  /*
   * call the "constructor" of the parent-class
   * this will reserve enough memory to hold "t_syncclockmulti"
   */
  t_syncclockmulti *x = (t_syncclockmulti *)pd_new(syncclockmulti_class);
  x->x_ticktime = 1.0;
  x->do_update = 1;

  // get arguments
  switch(argc){
  default:
  /* case 3: */
  /*   x->step=atom_getfloat(argv+2); */
  /* case 2: */
  /*   f2=atom_getfloat(argv+1); */
  case 1:
    x->f_freq_pref = atom_getfloat(argv);
    break;
  case 0:
    x->f_freq_pref = 2.0;
    break;
  }
  // if (argc<2)f2=f1;
  
  // preferred frequency
  x->f_period_pref = 1.0/x->f_freq_pref;
  
  x->f_freq = x->f_freq_pref;
  x->f_phi = 0.0;
  x->f_dphi = x->f_freq/1000.0;
  post("dphi = %f", x->f_dphi);
  x->f_period = 1.0/x->f_freq;
  x->f_period_in = 1.0/x->f_freq;

  x->f_eta_freq = 0.9;
  x->f_eta_phi = 0.9;
  
  /* create a new outlet for floating-point values */
  x->inlet_eta_freq = floatinlet_new(&x->x_obj, &x->f_eta_freq);
  x->inlet_eta_phi = floatinlet_new(&x->x_obj, &x->f_eta_phi);
  x->inlet_period = floatinlet_new(&x->x_obj, &x->f_period_in);

  // this becomes x->x_obj.ob_outlet
  // outlet_new(&x->x_obj, &s_float);
  
  x->outlet_clock = outlet_new(&x->x_obj, &s_bang);
  x->outlet_freq = outlet_new(&x->x_obj, &s_float);
  x->outlet_freq_err = outlet_new(&x->x_obj, &s_float);
  x->outlet_phi = outlet_new(&x->x_obj, &s_float);
  x->outlet_phi_err = outlet_new(&x->x_obj, &s_float);

  // clock init
  x->x_clock = clock_new(x, (t_method)syncclockmulti_tick);

  // schedule first tick
  clock_delay(x->x_clock, x->x_ticktime);
  
  /*
   * return the pointer to the class - this is mandatory
   * if you return "0", then the object-creation will fail
   */
  return (void *)x;
}

void syncclockmulti_free (t_syncclockmulti *x)
{
  inlet_free(x->inlet_eta_freq);
  inlet_free(x->inlet_eta_phi);
  // outlet_free(x->onsetbang);
  clock_free(x->x_clock);
}

/**
 * define the function-space of the class
 * within a single-object external the name of this function is special
 */
void syncclockmulti_setup(void) {
  /* create a new class */
  syncclockmulti_class = class_new(gensym("syncclockmulti"),        /* the object's name is "syncclockmulti" */
				   (t_newmethod)syncclockmulti_new, /* the object's constructor is "syncclockmulti_new()" */
				   (t_method)syncclockmulti_free,                           /* no special destructor */
				   sizeof(t_syncclockmulti),        /* the size of the data-space */
				   CLASS_DEFAULT,               /* a normal pd object */
				   A_GIMME,
				   0);                          /* no creation arguments */

  /* class_addmethod(syncclockmulti_class, */
  /*       (t_method)syncclockmulti_dsp, gensym("dsp"), A_CANT, 0); */

  /* attach functions to messages */
  /* here we bind the "syncclockmulti_bang()" function to the class "syncclockmulti_class()" -
   * it will be called whenever a bang is received
   */
  class_addbang(syncclockmulti_class, syncclockmulti_bang); 
}
