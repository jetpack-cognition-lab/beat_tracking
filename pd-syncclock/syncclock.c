/*
 * HOWTO write an External for Pure data
 * (c) 2001-2006 IOhannes m zm�lnig zmoelnig[AT]iem.at
 *
 * this is the source-code for the first example in the HOWTO
 * it creates an object that prints "Hello world!" whenever it 
 * gets banged.
 *
 * for legal issues please see the file LICENSE.txt
 */

/*
  *syncclock*: adaptive clock synchronizing to external clock
               given a stochastic input _clock_ signal
   *clockify*: turn any stochastic event stream into a clocklike
               stream with a minimum ioi (just miniioi, refractory)
 *eventmerge*: merge a bundle of events close in time into a
               single event

 *arguments*:
 - eta_freq
 - eta_phi
 - clockify on/off
 - perferred freq

 *test signals / input constraints*
 - regular clock pulse input
 - gaussian ioi clock pulse input
 - random clock pulse input
 - partial double time rhythmic input
 - partial triple and time rhythmic input
 */

/**
 * include the interface to Pd 
 */
#include "m_pd.h"
#include "math.h"

/**
 * define a new "class" 
 */
static t_class *syncclock_class;

/**
 * this is the dataspace of our new object
 * we don't need to store anything,
 * however the first (and only) entry in this struct
 * is mandatory and of type "t_object"
 */
typedef struct _syncclock {
  t_object  x_obj; // object reference
  t_clock *x_clock; // main clock
  double x_ticktime; // ticktime for phase check
  double f_phi; // current clock phase
  double f_phi_err; // current clock phase error
  float f_eta_phi; // clock phase update rate
  double f_dphi; // current clock phase change
  double f_freq; // current clock frequency
  double f_freq_err; // current clock frequency error
  float f_eta_freq; // clock frequency update rate
  double f_period; // current clock period
  double f_freq_pref; // local preferred clock frequency
  double f_period_pref; // local preferred current clock period
  
  // bangtime_last
  double time_of_last_bang_sec;
  // bangtime_cur
  double time_of_cur_bang_sec;
  // bangtime_delta
  double bang_to_bang_delta;

  // parameter inlets
  t_inlet *inlet_eta_freq;
  t_inlet *inlet_eta_phi;
  
  // t_outlet x_out_clock;
} t_syncclock;

/**
 * from https://stackoverflow.com/questions/4633177/c-how-to-wrap-a-float-to-the-interval-pi-pi
 * change to `float/fmodf` or `long double/fmodl` or `int/%` as appropriate
 */

/* wrap x -> [0,max) */
double wrapMax(double x, double max)
{
    /* integer math: `(max + x % max) % max` */
    return fmod(max + fmod(x, max), max);
}
/* wrap x -> [min,max) */
double wrapMinMax(double x, double min, double max)
{
    return min + wrapMax(x - min, max - min);
}

/**
 * this method is called whenever a "bang" is sent to the object
 * the name of this function is arbitrary and is registered to Pd in the 
 * syncclock_setup() routine
 */
void syncclock_bang(t_syncclock *x)
{
  /*
   * post() is Pd's version of printf()
   * the string (which can be formatted like with printf()) will be
   * output to wherever Pd thinks it has too (pd's console, the stderr...)
   * it automatically adds a newline at the end of the string
   */
  post("syncclock_bang, phi=%f", x->f_phi);
  
  // get current time in milliseconds
  x->time_of_cur_bang_sec = sys_getrealtime();
  
  // calculate beat period / period estimate
  x->bang_to_bang_delta = ((double) (x->time_of_cur_bang_sec - x->time_of_last_bang_sec))/1.0;

  /**
   * clockify
   * - improve freq measurement
   *  - event periodicity
   * - improve phase measurement
   *  - weed out spurious events
   *  - only allow events in a window of attention around a plausible phase
   *  - use ioi-histogram
   *  - use internal confidence
   */
  
  /* if (x->bang_to_bang_delta > (x->f_period_pref * 0.2) && x->bang_to_bang_delta < (x->f_period_pref * 0.8)) { */
  /*   // discard and return */
  /*   post("syncclock_bang, discarded bang_to_bang_delta = %f", x->bang_to_bang_delta); */
  /*   return; */
  /* } */

  // store time since last bang
  x->time_of_last_bang_sec = x->time_of_cur_bang_sec;
      
  /* // update freq prediction with error from freq measurement */
  /* x->f_freq_err = x->f_freq_pref - (1.0/x->bang_to_bang_delta); */

  /* if (fabs(x->f_freq_err / x->f_freq_pref) > 0.2) { */
  /*   post("syncclock_bang, discarded freq_err = %f", x->f_freq_err); */
  /*   return; */
  /* }     */

  // update error to current freq
  x->f_freq_err = x->f_freq - (1.0/x->bang_to_bang_delta);
  
  x->f_freq = x->f_freq - x->f_eta_freq * x->f_freq_err;
  post("syncclock_bang, f_freq=%f", x->f_freq);
  // update phase increment from freq
  x->f_dphi = x->f_freq/1000.0;
  post("syncclock_bang, f_dphi=%f", x->f_dphi);
  // update period
  x->f_period = 1.0/x->f_freq;

  // update instantaneous phase
  x->f_phi_err = x->f_phi - 0;
  if (x->f_phi_err < 0.5) {
    x->f_phi = x->f_phi - (x->f_eta_phi * x->f_phi_err);
  }
  else {
    x->f_phi = x->f_phi + (x->f_eta_phi * (1.0 - x->f_phi));
  }
  
  post("syncclock_bang, bperiod=%f", x->bang_to_bang_delta);
  // clock_delay(x->x_clock, x->x_ticktime);
}

/* void syncclock_dsp(t_syncclock *x, t_signal **sp) */
/* { */
/*   post("syncclock_dsp"); */
/*   /\* dsp_add(syncclock_perform, 5, x, *\/ */
/*   /\*         sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[0]->s_n); *\/ */
/* } */

static void syncclock_tick(t_syncclock *x)
{
  // post("syncclock tick");
  clock_delay(x->x_clock, x->x_ticktime);
  // outlet_bang(x->x_obj.ob_outlet);
  
  x->f_phi = x->f_phi + x->f_dphi;
  
  if(x->f_phi >= 1.0) {
    x->f_phi = wrapMinMax(x->f_phi, 0.f, 1.f);
    outlet_bang(x->x_obj.ob_outlet);
    // x->f_phi = x->f_phi - 1.0;
  }
}

/**
 * this is the "constructor" of the class
 * this method is called whenever a new object of this class is created
 * the name of this function is arbitrary and is registered to Pd in the 
 * syncclock_setup() routine
 */
void *syncclock_new(void)
{
  /*
   * call the "constructor" of the parent-class
   * this will reserve enough memory to hold "t_syncclock"
   */
  t_syncclock *x = (t_syncclock *)pd_new(syncclock_class);
  x->x_ticktime = 1.0;

  // preferred frequency
  x->f_freq_pref = 2.0;
  x->f_period_pref = 1.0/x->f_freq_pref;
  
  x->f_freq = x->f_freq_pref;
  x->f_phi = 0.0;
  x->f_dphi = x->f_freq/1000.0;
  post("dphi = %f", x->f_dphi);
  x->f_period = 1.0/x->f_freq;

  x->f_eta_freq = 0.9;
  x->f_eta_phi = 0.9;
  
  /* create a new outlet for floating-point values */
  x->inlet_eta_freq = floatinlet_new(&x->x_obj, &x->f_eta_freq);
  x->inlet_eta_phi = floatinlet_new(&x->x_obj, &x->f_eta_phi);
  outlet_new(&x->x_obj, &s_float);

  // clock init
  x->x_clock = clock_new(x, (t_method)syncclock_tick);

  // schedule first tick
  clock_delay(x->x_clock, x->x_ticktime);
  
  /*
   * return the pointer to the class - this is mandatory
   * if you return "0", then the object-creation will fail
   */
  return (void *)x;
}

void syncclock_free (t_syncclock *x)
{
  inlet_free(x->inlet_eta_freq);
  inlet_free(x->inlet_eta_phi);
  // outlet_free(x->onsetbang);
  clock_free(x->x_clock);
}

/**
 * define the function-space of the class
 * within a single-object external the name of this function is special
 */
void syncclock_setup(void) {
  /* create a new class */
  syncclock_class = class_new(gensym("syncclock"),        /* the object's name is "syncclock" */
			      (t_newmethod)syncclock_new, /* the object's constructor is "syncclock_new()" */
			      (t_method)syncclock_free,                           /* no special destructor */
			      sizeof(t_syncclock),        /* the size of the data-space */
			      CLASS_DEFAULT,               /* a normal pd object */
			      0);                          /* no creation arguments */

  /* class_addmethod(syncclock_class, */
  /*       (t_method)syncclock_dsp, gensym("dsp"), A_CANT, 0); */

  /* attach functions to messages */
  /* here we bind the "syncclock_bang()" function to the class "syncclock_class()" -
   * it will be called whenever a bang is received
   */
  class_addbang(syncclock_class, syncclock_bang); 
}
